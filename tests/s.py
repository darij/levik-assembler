# -*- coding: cp1251 -*-

import unittest
from StringIO import StringIO
import os, sys
sys.path[0] = os.path.split(sys.path[0])[0]
from s import Arch, Instruction, ArchFunc, bin, cbin, tobits, toint


class TestInstruction(unittest.TestCase):
    def testBin(self):
        i = toint('0x1E')
        self.assertEqual(i, 0x1E)
        i = toint('0b1011')
        self.assertEqual(i, 0b1011)
        i = toint('0777')
        self.assertEqual(i, 0777)
        i = toint('36rXYZ')
        self.assertEqual(i, 33*36*36 + 34*36 + 35)
    
        s = tobits('AF', radix=16, bit_radix=2)
        self.assertEqual(s, '10101111')
    
        s = bin(10, 6)
        self.assertEqual(s, '001010')
        s = bin(12, size=3, radix=16)
        self.assertEqual(s, '00C')
        
    def testI86Bin(self):
        # ��� ����������� 8 ��� � �����, ���=0 ��� 1
        # ��� ���������� 6 ��� � �����, ���=0 ��� 1 ��� 2, ����� ��������� ���:
        # s = cbin()
        s = cbin('0010000000100000', size_byte=8)
        self.assertEqual(s, '  ')

    def testString(self):
        arch = Arch('test')
        s = arch.tostring(r"S\nA")
        self.assertEqual(s, '01010011'+'00001010'+'01000001')
        
    def testInstruction(self):
        instruction = Instruction('  (2:1010)[1:4](2R1010)[2:4:2,6!][3+0x1:2][4-3+5:6]   movl   c,  m,  m, c  ')
        
        self.assert_(isinstance(instruction, Instruction))
        self.assert_(instruction.arch.instructions.has_key('movl c, m, m, c'))
        self.assertEqual(instruction.count_args, 4)
        self.assertEqual(instruction.count_bytes(), 3)
        self.assertEqual(instruction.count_bits(), 24)
        # instruction.mask == ['1010', ArchFunc(1, 4), '1010', ArchFunc(2, 4, 2, 6, True)]
        self.assertEqual(instruction.mask[0], '1010')
        self.assertEqual(instruction.mask[1].narg, 0)
        self.assertEqual(instruction.mask[2], '1010')
        self.assertEqual(instruction.mask[3].count_bits, 4)
        self.assertEqual(instruction.mask[3].from_bit, 2)
        self.assertEqual(instruction.mask[3].to_bit, 6)
        self.assert_(instruction.mask[3].relative_offset)
        self.assertEqual(instruction.mask[4].count_bits, 2)
        self.assertEqual(instruction.mask[4].expression, [('+', 1)])
        self.assertEqual(instruction.mask[5].count_bits, 6)
        self.assertEqual(instruction.mask[5].expression, [('-', 3), ('+', 5)])
        

    def testRegs(self):
        arch = Arch()
        arch.add_reg('reg16', 'ax', 0)
        arch.add_reg('reg16', 'bx', 1)
        
        self.assertEqual(arch.regs, { 'ax':(0, 'reg16'), 'bx':(1, 'reg16') } )
        
        instruction = Instruction('  (2:1010)[1:4](2R1010)[2:4:2,6!]   movl   c,  reg16  ', arch)
        mnemonic, args = arch.precompil_instruction('  movl  $-10,  %bx ')
        
        self.assertEqual(mnemonic, 'movl c, reg16')
        self.assertEqual(args, [-10, 1])
        
        
    def testAsm(self):
        arch = Arch()
        arch.include('test')
        
        self.assertEqual(arch.count_bytes, 0)
        
        arch.count_bytes = 45
        arch.set_label('x')
        
        self.assertEqual(arch.labels['x'], 45)
        
        mnemonic, args = arch.precompil_instruction('  addl  $-10,  x ')
        
        self.assertEqual(args, [-10, 'x'])
        self.assertEqual(mnemonic, 'addl c, m')
        self.assertEqual(arch.count_bytes, 45)

    def testCompil(self):        
        in_str = StringIO()
        out_str = StringIO()
        in_str.write('''
            .arch test
            m:
                movl 10, m
            z:
                movl $10, m
        ''')
        in_str.name = "[memoryfile]"
        in_str.seek(0)

        arch = Arch()
        arch.fcompil(in_str, out_str)
        
        self.assertEqual(arch.count_bytes, 16)
        self.assertEqual(arch.labels, {'m':0, 'z':4})
        self.assertEqual(out_str.getvalue(), '0000101010101010000000000000111110101111000000000000101010111100')

    def testCCompil(self):
        try:
            arch = Arch()
            arch.ccompil('test.s', 'test.com')
        except BaseException, e:
            import traceback
            traceback.print_exc()
            print arch.lineno+1,":", arch.line
            raise
        
if __name__ == '__main__':
    unittest.main()


