# -*- coding: cp1251 -*-

"""
���������� �����:                    dw
����� ��������� �������� �����:      dw
���������� ������� � �����:          dw
���. ��������� ����. �����������:    dw
������ ��������� � ����������:       dw
����������� ������ ��� ���������:    dw
������������ ������ ��� ���������:   dw
�������� ������ ����� SS:SP:         dw:dw
����������� �����:                   dw
�������� ��� ��������� CS:IP:        dw:dw
�������� ����. �����������:          dw
����� �������:                       dw
"""

class Format:
    def __init__(self, arch):
        self.arch = arch       
        self.segments = []
        
        self.header = arch.tostring("MZ")
        self.max_alloc_mem = 0
        self.min_alloc_mem = 0
        self.stack_size = 0
        self.ss_reg = 0
        self.sp_reg = 0
        self.ip_reg = 0
        self.cs_reg = 0
        self.prog_offset = 0
        self.overlay = 0
        
    def segment(self, name, args):
        from s import FormatException
        if name == 'bss':
            assert len(args) in (1, 2)
            if len(args)==1:
                self.max_alloc_mem = int(args.keys()[0])
                self.min_alloc_mem = self.max_alloc_mem
            else:
                self.max_alloc_mem = int(args['max'])
                self.min_alloc_mem = int(args['min'])
            return
        if name == 'stack':
            assert len(args) == 1
            self.stack_size = int(args['size'])
            return
        if name == 'overlay':
            assert len(args) == 1
            self.overlay = int(args.keys()[0])
            return

        if args.has_key('align'):
            align = int(args['align'])
            paragraph_offset = self.arch.count_bytes % align
            if paragraph_offset:
                paragraph_offset = align - paragraph_offset
            self.segments.append(self.arch.count_bytes + paragraph_offset)
            return self.arch.tobin(0, paragraph_offset)
        
    def code_end(self):
        arch = self.arch       
        last_block_size = arch.count_bytes % 512
        blocks = arch.count_bytes / 512
        if last_block_size:
            blocks += 1
        self.header = (
          arch.tobin(last_block_size, 2)
        + arch.tobin(blocks, 2)
        + arch.tobin(0, 2)
        + arch.tobin(len(self.segments), 2)
        + arch.tobin(self.min_alloc_mem, 2)
        + arch.tobin(self.max_alloc_mem, 2)
        + arch.tobin(self.ss_reg, 2)
        + arch.tobin(self.sp_reg, 2)
        + arch.tobin(0, 2) # ����������� ����� �� ���������
        + arch.tobin(self.ip_reg, 2)
        + arch.tobin(self.cs_reg, 2)
        + arch.tobin(self.prog_offset, 2)
        + arch.tobin(self.overlay, 2)
        )
        