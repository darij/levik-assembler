# -*- coding: cp1251 -*-

import sys
reload(sys)
sys.setdefaultencoding('cp1251')
import codecs
sys.stdout = codecs.getwriter('cp866')(sys.stdout, errors='replace')
sys.stderr = codecs.getwriter('cp866')(sys.stderr, errors='replace')

from struct import unpack

f = open(sys.argv[1], 'rb')

header = tuple(unpack("H", f.read(2))[0] for x in xrange(14))

print """
���������� �����:                    %04Xh
����� ��������� �������� �����:      %i
���������� ������� � �����:          %i
���. ��������� ����. �����������:    %i
������ ��������� � ����������:       %i
����������� ������ ��� ���������:    %04Xh
������������ ������ ��� ���������:   %04Xh
�������� ������ ����� SS:SP:         %04X:%04Xh
����������� �����:                   %04Xh
�������� ��� ��������� CS:IP:        %04X:%04Xh
�������� ����. �����������:          %02Xh
����� �������:                       %i,
""" % header

#print "\n���������� ������� �����������:\n\n"

#for i in xrange(header[3]):
#    print "%04X:%04X" % (unpack("H", f.read(2))[0], unpack("H", f.read(2))[0])
